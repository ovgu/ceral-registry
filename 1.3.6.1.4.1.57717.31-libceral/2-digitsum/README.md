# Digit Sum

Calculates the sum of all digits in the base-10 representation of a number.

## Data Types
### Parameter
    unsigned int

The number to calculate the digit sum for.

CBOR:
- Major Type 0 (unsigned integer)

### Value
    unsigned int

The sum of all digits in the base-10 representation of the parameter.

CBOR:
- Major Type 0 (unsigned integer)

### Witness
    unsigned char[11]
      <digit value of the nth-last digit>

A byte array containing each digit in "big endian" order (i.e., the rightmost digit comes first).
For the input `123`, this value would be `[3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0]` - the last element is always zero.

CBOR:
- Major Type 2 (byte string) with length 11
