# Public Registry for Certifying Algorithms

This registry contains specifications for the *skeletons* of certifying algorithms according to the master thesis "Interoperable zertifizierende Algorithmen auf eingebetteten Systemen".

A skeleton specifies what an algorithm should do, together with the data types used for the parameters, return value and witness, as well as the serialization format as CBOR. Each skeleton gets an OID assigned - you can freely use the namespace `1.3.6.1.4.1.57717.31` - only the children that exists in this repository exist there, but you should pay attention to open pull requests to avoid duplicates.

Helper functions for data types and serialization can be provided in a subdirectory, using the programming language as a directory name.

Each skeleton should be specified in a `README.md` file, using the following syntax:

```markdown
# Algorithm Name

Short description what the algorithm does.

## Data Types
### Parameter
    unsigned int

Short description what the parameter looks like

CBOR:
- Major Type 0 (unsigned integer)

### Value
    unsigned char (< 128)

Short description what the return value looks like.

CBOR:
- Major Type 0 (unsigned integer)

### Witness
    unsigned char[11]
      <digit value of the nth-last digit>

Short description what the return value looks like.

CBOR:
- Major Type 2 (byte string)
```
