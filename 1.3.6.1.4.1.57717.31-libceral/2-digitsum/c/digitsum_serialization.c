#include <stdlib.h>
#include <ceral.h>

CborError digitsum_encode_params(CborEncoder *encoder, void *params) {
    return cbor_encode_int(encoder, *((int* ) params));
}
CborError digitsum_decode_params(CborValue *encoded_value, void **params) {
    *params = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)params);
}
CborError digitsum_encode_value(CborEncoder *encoder, void *value) {
    return cbor_encode_int(encoder, *((int*) value));
}
CborError digitsum_decode_value(CborValue *encoded_value, void **value) {
    *value = malloc(sizeof(int));
    return cbor_value_get_int(encoded_value, *(int **)value);
}
CborError digitsum_encode_witness(CborEncoder *encoder, void *witness) {
    return cbor_encode_byte_string(encoder, (uint8_t *) witness, 11);
}
CborError digitsum_decode_witness(CborValue *encoded_value, void **witness) {
    *witness = malloc(11 * sizeof(uint8_t));
    size_t size = 11;
    CborValue noop;
    return cbor_value_copy_byte_string(encoded_value, *(uint8_t **)witness, &size, &noop);
}
